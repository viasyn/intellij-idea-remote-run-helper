#!/bin/env python3
import sys
import os
import time
import shutil
import shlex, subprocess
import stat
import collections
import threading

# Usage:
# 1) mv $JAVA_HOME/bin/java $JAVA_HOME/bin/java.bin
# 2) ln -s /path/to/this.script $JAVA_HOME/bin/java
# 3) Run tests with params like '-Dremote.run.host=ubuntu -Dremote.run.shared.local=/home/viasyn/work/vtmp -Dremote.run.shared.remote=/media/sf_work/vtmp'

# Working features:
# [+] Scalatests under IDEA (with debugger)
# [+] Testng under IDEA (with debugger)

# TODO:
# [?] Server mode of debugger is untested
# [?] -agentlib support is untested
# [?] JUnit untested
# [-] Sophisticated host/port mapper, able to extract data from any unknown argument/file
# [-] ipv6 not supported
# [-] only passwordless login with current uid supported for now
# [-] Smart linking may be used instead of copying (it's good idea to create repository of
#      files on same filesystem as shared folder and link them), mount --bind may be also useful


def run_local_java(args):
    ownpath = os.path.dirname(os.path.abspath(__file__))
    javapath = os.path.join(ownpath, "java.bin")
    sys.stdout.flush()
    os.execv(javapath, args)

class NotRemoteRunException(Exception):
    def __init__(self):
        Exception.__init__(self, "It's not remote run")

class TestNgMonitor(threading.Thread):
     def __init__(self, runner, temp_path, target_path):
         super(TestNgMonitor, self).__init__()
         self.runner = runner
         self.temp_path = temp_path
         self.target_path = target_path

     def read_file(self):
        with open (self.temp_path, "r") as f:
            return f.read()


     def run(self):
        content = ""
        while not content.endswith("end"):
            time.sleep(0.1)
            try:
                content = self.read_file()
            except Exception as e:
                print(e)
                sys.stdout.flush()
                pass
        print("Got testng content:\n%s" %content)
        sys.stdout.flush()

        content2 = []
        for suite in content.split('\n'):
            if "end" != suite:
                content2.append("/"+self.runner.copy_file_to_shared_temp_dir(suite))
            else:
                content2.append("end")

        content2 = '\n'.join(content2)
        print("Writing content => %s:\n%s" % (self.target_path, content2))
        sys.stdout.flush()

        with open(self.target_path, "w+") as runfile:
            print(content2, file=runfile)

class RemoteJavaRunner(object):
    def __init__(self, args):
        self.java_command = args[0]
        self.args = args[1:]
        defines = self.get_defines(args)
        if not ("remote.run.host" in defines and "remote.run.shared.local" in defines and "remote.run.shared.remote" in defines):
            raise NotRemoteRunException()

        runid = "rerun-%d" % int(time.time()*1000000)

        self.remote_host = defines["remote.run.host"]
        self.shared_dir = defines["remote.run.shared.local"]
        self.remote_shared_dir = defines["remote.run.shared.remote"]
        self.shared_temp_dir = os.path.join(self.shared_dir, runid)
        self.remote_shared_temp_dir = os.path.join(self.remote_shared_dir, runid)
        os.makedirs(self.shared_temp_dir)

        self.ports_to_map = []
        self.ports_to_reverse_map = []

        self.counter_linked = 0
        self.counter_copied = 0
        self.counter_dirs = 0

        self.cwd = '$(dirname $(readlink -f "$0"))'

        idea_port = defines.get("idea.launcher.port")
        if idea_port:
            self.ports_to_map.append(('localhost', idea_port))

    def run_remote_java(self):
        modified_args = [self.process_arg(idx, arg) for idx, arg in enumerate(self.args)]

        command = "java %s" % ' '.join(modified_args)
        runscript = self.path_local("run.sh")
        with open(runscript, "w+") as runfile:
            print('#/bin/sh\ncd %s\n%s' % (self.cwd, command), file=runfile)
        self.chmod_x(runscript)
        command = self.path_remote("run.sh")

        port_mappings = [self.define_port_mapping(host, port) for host, port in self.ports_to_map]
        port_reverse_mappings = [self.define_reverse_port_mapping(host, port) for host, port in self.ports_to_reverse_map]

        ssh_command = "ssh %(port_mappings)s %(port_reverse_mappings)s %(remote_host)s -- %(command)s" % {
            'port_mappings': ' '.join(port_mappings),
            'port_reverse_mappings': ' '.join(port_reverse_mappings),
            'remote_host': self.remote_host,
            'command': command,
        }

        print("="*80)
        print("Linked files: %d, copied files: %d, copied dirs: %s" % (self.counter_linked, self.counter_copied, self.counter_dirs))
        print('$', ssh_command)
        print("="*80)
        print()
        sys.stdout.flush()
        proc = subprocess.Popen(shlex.split(ssh_command), shell=False)
        proc.communicate()
        print("="*80)

        if proc.returncode != 0:
            print("Unexpected return code: %d, temp directory not removed: %s" % (proc.returncode, self.shared_temp_dir))
        else:
            shutil.rmtree(self.shared_temp_dir)
            pass

        exit(proc.returncode)

    def process_arg(self, idx, arg):
        if arg == "org.testng.RemoteTestNGStarter":
            self.isTestNg = True
        testng = getattr(self, "isTestNg", False)

        if idx > 0 and self.args[idx-1] == '-classpath':
            return self.process_cp(arg)
        elif arg.startswith("-agentlib:jdwp"):
            return self.process_jdwp(arg)
        elif arg.startswith('-javaagent') or arg.startswith('-agentpath'):
            return self.process_agent(arg)
        elif testng and idx > 0 and self.args[idx-1] == '-temp':
            path = self.path_local(arg[1:])

            try:
                os.makedirs(os.path.dirname(path))
            except:
                pass

            with open(path, "w+") as runfile:
                print("", file=runfile)
            monitor = TestNgMonitor(self, arg, path)
            monitor.start()
            return self.path_remote(arg[1:])
        elif testng and idx > 0 and self.args[idx-1] == '-port':
            self.ports_to_reverse_map.append(('localhost', arg))
            return arg
        elif testng and arg.startswith("-socket"):
            self.ports_to_reverse_map.append(('localhost', arg.replace("-socket", "")))
            return arg

        return arg

    def process_jdwp(self, arg):
        #-agentlib:jdwp=transport=dt_socket,address=127.0.0.1:41117,suspend=y,server=n
        agentargs = arg.split("=")
        prefix = agentargs[0]
        param_string = '='.join(agentargs[1:])
        params = collections.OrderedDict(kv.split("=") for kv in param_string.split(','))
        address = params.get("address")
        server = params.get("server")
        host, port = address.split(":")
        if server.lower() == "y":
            self.port_mappings.append((host, port))
        else:
            self.ports_to_reverse_map.append((host, port))

        return "%s=%s" % (prefix, ','.join(["%s=%s" % (k,v) for k, v in params.items()]))

    def process_agent(self, arg):
        prefix, path = arg.split(":")
        return ':'.join([prefix, self.copy_file_to_shared_temp_dir(path)])

    def process_cp(self, cp):
        cpes = cp.split(":")
        ret=[]
        for c in cpes:
            if c:
                remote_path = self.copy_file_to_shared_temp_dir(c)
                ret.append(remote_path)
        return ":".join(ret)

    def copy_file_to_shared_temp_dir(self, src):
        dst = self.path_local(src[1:])
        try:
            os.makedirs(os.path.dirname(dst))
        except:
            pass

        try:
            os.link(src, dst)
            self.counter_linked += 1
        except Exception as e:
            #print("Can't link, so copying: %s => %s" % (src, dst))
            if os.path.isdir(src):
                shutil.copytree(src, dst)
                self.counter_dirs += 1
            else:
                shutil.copy2(src, dst)
                self.counter_copied += 1
        return self.path_remote(src[1:])

    def path_local(self, filename):
        return os.path.join(self.shared_temp_dir, filename)

    def path_remote(self, filename):
        return os.path.join(self.remote_shared_temp_dir, filename)

    @staticmethod
    def get_defines(args):
        defines = [a[2:] for a in args if a.startswith("-D")]
        def_pairs = []
        for d in defines:
            parts = d.split("=")
            k = parts[0]
            v = "=".join(parts[1:])
            pair = (k, v)
            def_pairs.append(pair)
        defines_map = dict(def_pairs)
        return defines_map

    @staticmethod
    def define_port_mapping(host, port):
        return "-L %(port)s:%(host)s:%(port)s" % {'host': host, 'port': str(port)}

    @staticmethod
    def define_reverse_port_mapping(host, port):
        return "-R %(port)s:%(host)s:%(port)s" % {'host': host, 'port': str(port)}

    @staticmethod
    def chmod_x(filename):
        st = os.stat(filename)
        os.chmod(filename, st.st_mode | stat.S_IEXEC)


if __name__ == "__main__":
    try:
        RemoteJavaRunner(sys.argv).run_remote_java()
    except NotRemoteRunException as e:
        run_local_java(sys.argv)


